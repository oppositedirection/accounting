import React from "react";
import { Modal, Button, ButtonToolbar } from "react-bootstrap";
import Sales from "./sales";

function ModalAdd(props) {
  const isSales = true;

  if (isSales) {
    return (
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Name of View
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Sales />
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Name of View
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>isNotSales</div>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}

function Add() {
  const [modalShow, setModalShow] = React.useState(false);

  return (
    <ButtonToolbar>
      <Button variant="primary" onClick={() => setModalShow(true)}>
        Add
      </Button>

      <ModalAdd show={modalShow} onHide={() => setModalShow(false)} />
    </ButtonToolbar>
  );
}

export default Add;
