import React from "react";
import { Table } from "react-bootstrap";

function AddItem() {
  const colNames = ["Qt", "Item", "Price", "Tax", "Amount"];
  const data = [
    {
      qt: 1,
      item: "WOW20203 Washer",
      price: 125,
      tax: 25,
      amount: 150
    }
  ];

  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          {colNames.map(col => (
            <th>{col}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {data.map(row => (
          <tr>
            <td>{row.qt}</td>
            <td>{row.item}</td>
            <td>{row.price}</td>
            <td>{row.tax}</td>
            <td>{row.amount}</td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
}

export default AddItem;
