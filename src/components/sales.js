import React from "react";
import { Form, Col, Button } from "react-bootstrap";
import AddItem from "./additem";

function Sales() {
  return (
    <Form>
      <Form.Row>
        <Form.Group as={Col} controlId="formGridEmail">
          <Form.Control
            type="text"
            placeholder="Newbury Appliance Outlet"
            autoComplete="new-password"
          />
        </Form.Group>

        <Form.Group as={Col} controlId="formGridPassword">
          <Form.Control
            type="text"
            placeholder="Invoice"
            autoComplete="new-password"
          />
        </Form.Group>
      </Form.Row>

      <Form.Group controlId="formGridAddress1">
        <Form.Label>Bill To</Form.Label>
        <Form.Control placeholder="1234 Main St" autoComplete="new-password" />
      </Form.Group>

      <Form.Group controlId="formGridAddress2">
        <Form.Label>Address 2</Form.Label>
        <Form.Control
          placeholder="Apartment, studio, or floor"
          autoComplete="new-password"
        />
      </Form.Group>

      <Form.Row>
        <Form.Group as={Col} controlId="formGridCity">
          <Form.Label>City</Form.Label>
          <Form.Control autoComplete="new-password" />
        </Form.Group>

        <Form.Group as={Col} controlId="formGridState">
          <Form.Label>State</Form.Label>
          <Form.Control as="select">
            <option>MA</option>
            <option>NC</option>
          </Form.Control>
        </Form.Group>

        <Form.Group as={Col} controlId="formGridZip">
          <Form.Label>Zip</Form.Label>
          <Form.Control autoComplete="new-password" />
        </Form.Group>
      </Form.Row>

      <AddItem />

      <Button variant="primary" type="submit">
        Create
      </Button>
    </Form>
  );
}

export default Sales;
