import React from "react";
import { Link } from "react-router-dom";
import "./card.css";

function Card() {
  const cards = {
    sales: {
      name: "Sales",
      period: "this week",
      subheader: "$25,000 | AVG $200.98",
      amount: 30,
      units: " Units",
      comparison: "+120%",
      period_selector: "1W 1M 2M All"
    },
    purchases: {
      amount: 1500,
      name: "Purchases",
      chart: "This Chart"
    },
    tools: {
      amount: 3000,
      name: "Tools"
    }
  };
  return (
    <div className="card_container">
      {Object.keys(cards).map(key => (
        <div key={key} className="card">
          <Link to={`/app/${cards[key].name}`}>
            <div className="name ">{cards[key].name}</div>
          </Link>
          <div className="period">{cards[key].period}</div>
          <div className="subheader"> {cards[key].subheader} </div>
          <div className="amount"> {cards[key].amount}</div>
          <div className="units"> {cards[key].units}</div>
          <div className="comparison"> {cards[key].comparison}</div>
          <div className="period_selector">{cards[key].period_selector}</div>
        </div>
      ))}
    </div>
  );
}

export default Card;
