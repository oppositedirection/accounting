import React from "react";
import Card from "./card";
import Add from "./add";

class Dashboard extends React.Component {
  render() {
    return (
      <div>
        <Add />
        <Card />
      </div>
    );
  }
}

export default Dashboard;
