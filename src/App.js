import React from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Login from "./components/login";
import Dashboard from "./components/dashboard";
import ListView from "./components/listview";
import NoMatch from "./components/noMatch";
import { Navbar } from "react-bootstrap";

function App() {
  return (
    <div>
      <Router>
        <Navbar bg="dark" variant="dark">
          <Navbar.Brand href="/app/dashboard">
            <img
              alt=""
              src="/logo.svg"
              width="30"
              height="30"
              className="d-inline-block align-top"
            />
            {" Company Name "}
          </Navbar.Brand>
        </Navbar>

        <div className="router">
          <Switch>
            <Route path="/app" exact component={Login} />
            <Route path="/app/dashboard" component={Dashboard} />
            <Route path="/app/listview" component={ListView} />
            <Route component={NoMatch} />
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
